<?php
Route::view('/', 'home')->name('home');
Route::view('/conceptos', 'conceptos')->name('conceptos');
Route::view('/sucursales', 'sucursales')->name('sucursales');
Route::view('/usuarios', 'usuarios')->name('usuarios');

//Route::post('conceptos', 'FormularioConceptos@store');
//Route::post('sucursales', 'FormularioSucursales@store');
//Route::post('usuarios', 'FormularioUsers@store');
//Route::post('usuarios', 'FormularioUsers@mensajes');

Route::get('usuarios/create',['as'=>'usuarios.create', 'uses' => 'FormularioUsers@create']);
Route::get('sucursales/create',['as'=>'sucursales.create', 'uses' => 'FormularioSucursales@create']);
Route::get('conceptos/create',['as'=>'conceptos.create', 'uses' => 'FormularioConceptos@create']);


//Agregar a la base de datos
Route::post('usuarios',['as'=>'usuarios.store', 'uses' => 'FormularioUsers@store']);
Route::post('sucursales',['as'=>'sucursales.store', 'uses' => 'FormularioSucursales@store']);
Route::post('conceptos',['as'=>'conceptos.store', 'uses' => 'FormularioConceptos@store']);

//Mostrar en la base de datos
Route::get('usuarios',['as'=>'usuarios.index', 'uses' => 'FormularioUsers@index']);
Route::get('sucursales',['as'=>'sucursales.index', 'uses' => 'FormularioSucursales@index']);
Route::get('conceptos',['as'=>'conceptos.index', 'uses' => 'FormularioConceptos@index']);


//Mostrar solo un dato
Route::get('usuarios/{id}',['as'=>'usuarios.show', 'uses' => 'FormularioUsers@show']);
Route::get('sucursales/{id}',['as'=>'sucursales.show', 'uses' => 'FormularioSucursales@show']);
Route::get('conceptos/{id}',['as'=>'conceptos.show', 'uses' => 'FormularioConceptos@show']);