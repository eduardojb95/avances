@extends('layout')
@section('content')
	<h1>Todos los mensajes</h1>
	<table width="100%" border="1">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Saldo</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($sucursales as $sucursal)
			<tr>
				<td>{{ $sucursal->nombre }}</td>
				<td>{{ $sucursal->saldo }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop