@extends('layout')
@section('title','Sucursales')
@section('content')
	<h1>Sucursales</h1>
	@if(session()->has('info'))
	<h3>{{session('info')}}</h3>
@else
	<form method="POST" action="{{ route('sucursales.store') }}">
		@csrf
		<p><label>
		<input name="nombre" placeholder="Ingresa el nombre de la sucursal" value="{{ old('nombre') }}"><br>
		{!! $errors->first('nombre', '<small>:message</small><br>')!!}
		</label></p>
		<p><label>
		<input name="saldo" placeholder="Ingresa el saldo de la sucursal" value="{{ old('saldo') }}"><br>
		{!! $errors->first('saldo', '<small>:message</small><br>')!!}
		</label></p>
		<button>Enviar</button>
	</form>
	@endif

@endsection