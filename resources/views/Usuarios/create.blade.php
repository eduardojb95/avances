@extends('layout')
@section('title','Usuarios')
@section('content')
<h1>Usuarios</h1>
@if(session()->has('info'))
<h3>{{session('info')}}</h3>
@else
<form method="POST" action="{{ route('usuarios.store') }}">
	@csrf
	<p><label>
		<input name="name" placeholder="Ingresa nombre" value="{{ old('name') }}"><br>
		{!! $errors->first('name', '<small>:message</small><br>')!!}
	</label></p>
	<p><label>
		<input type="text" name="email" placeholder="Ingresa el email" value="{{ old('email') }}"><br>
		{!!$errors->first('email', '<small>:message</small><br>')!!}
	</label></p>
	<p><label>
		<input type="password" name="password" placeholder="Ingresa contraseña" value="{{ old('password') }}"><br>
		{!!$errors->first('password', '<small>:message</small><br>')!!}
	</label></p>
	<button>Enviar</button>
</form>
@endif
@endsection