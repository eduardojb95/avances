@extends('layout')
@section('content')
	<h1>Todos los mensajes</h1>
	<table width="100%" border="1">
		<thead>
			<tr>
				<th>Nombre</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($usuarios as $usuario)
			<tr>
				<td>{{ $usuario->name }}</td>
				<td>{{ $usuario->email }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop