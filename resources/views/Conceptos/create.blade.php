@extends('layout')
@section('title','Conceptos')
@section('content')
<h1>Conceptos</h1>
@if(session()->has('info'))
<h3>{{session('info')}}</h3>
@else
<form method="POST" action="{{ route('conceptos.store') }}">
	@csrf
	<p><label>
	<input name="cuenta" placeholder="Ingresa el numero de cuenta" value="{{ old('cuenta') }}"><br>
	{!! $errors->first('cuenta', '<small>:message</small><br>')!!}
	</label></p>
	<p><label>
	<input name="concepto" placeholder="Ingresa el concepto" value="{{ old('concepto') }}"><br>
	{!! $errors->first('concepto', '<small>:message</small><br>')!!}
	</label></p>
	<button>Enviar</button>
</form>
@endif
@endsection