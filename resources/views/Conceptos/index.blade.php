@extends('layout')
@section('content')
	<h1>Todos los mensajes</h1>
	<table width="100%" border="1">
		<thead>
			<tr>
				<th>Cuenta</th>
				<th>Cocepto</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($conceptos as $concepto)
			<tr>
				<td>{{ $concepto->cuenta }}</td>
				<td>{{ $concepto->concepto }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@stop